import { Component, OnInit, NgZone } from '@angular/core';
import { TareaService } from '../shared/tarea.service'
import {FormBuilder, FormGroup} from '@angular/forms'
import{ Router }from '@angular/router'

@Component({
  selector: 'app-crear-tarea',
  templateUrl: './crear-tarea.component.html',
  styleUrls: ['./crear-tarea.component.css']
})
export class CrearTareaComponent implements OnInit {
  title: string= "Registro de tarea";
  tareaForm: FormGroup;
  TareaArr: any=[];

  ngOnInit(){
    this.addTarea();
  }
  constructor(
    public fb:FormBuilder,
    private NgZone: NgZone,
    private router: Router,
    public servicioTarea: TareaService
  ) { }

  addTarea(){
    this.tareaForm=this.fb.group({
      value:[''],
      fec_inicio:[''],
      fec_vencida:[''],
      id_user_tar:[''],
      estado:['']
    })
  }

  submitForm(){
    this.servicioTarea.CrearTarea(this.tareaForm.value).subscribe(res=>{
      console.log('Tarea Agregada')
      this.NgZone.run(()=>this.router.navigateByUrl(''))
    });
  }

}
