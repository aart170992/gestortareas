import { Component, OnInit, NgZone } from '@angular/core';
import { TareaService } from '../shared/tarea.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editar-tarea',
  templateUrl: './editar-tarea.component.html',
  styleUrls: ['./editar-tarea.component.css']
})
export class EditarTareaComponent implements OnInit {
  EditarList: any=[];
  ActualizarForm: FormGroup;
  constructor(
    private actRoute: ActivatedRoute,
    public tareaService: TareaService,
    public fb: FormBuilder,
    private ngZone: NgZone,
    private router:Router
  ) { 
    var id=this.actRoute.snapshot.paramMap.get('para');
    this.tareaService.TodasTarea(id).subscribe((data)=>{
      this.ActualizarForm=this.fb.group({
        id:[data.id],
        value:[data.value],
        //fec_inicio:[data.fec_inicio],
        fec_vencida:[data.fec_vencida],
        //fec_final:[data.fec_final],
        //id_user_tar:[data.id_user_tar],
        estado:[data.estado]
      })
    })
  }

  ngOnInit() {
    this.Actualizartarea();
  }

  Actualizartarea(){
    this.ActualizarForm=this.fb.group({
      //id:[''],
      id:[''],
      value:[''],
      fec_vencida:[''],
      estado:['']


    })
  }
  submitForm(){ 
    var id = this.actRoute.snapshot.paramMap.get('para');
    this.tareaService.ActualizarTarea(this.ActualizarForm.value).subscribe(res => {
      this.ngZone.run(() => this.router.navigateByUrl('/obtenerTarea'))
    })
  }
}
 