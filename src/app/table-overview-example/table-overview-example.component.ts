import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {TareaService} from '../shared/tarea.service'
import { tarea } from '../shared/tarea';

  export interface UserData {
    id: string;
    name: string;
    progress: string;
    color: string;
  }
  
  /** Constants used to fill up our data base. */
  const COLORS: string[] = [
    'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
    'aqua', 'blue', 'navy', 'black', 'gray'
  ];
  const NAMES: string[] = [
    'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
    'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
  ];
  
  /**
   * @title Data table with sorting, pagination, and filtering.
   */
  @Component({
    selector: 'app-table-overview-example',
    templateUrl: './table-overview-example.component.html',
    styleUrls: ['./table-overview-example.component.css']
  })
  
  export class TableOverviewExampleComponent implements OnInit {
    
    datos:any=[];
    displayedColumns: string[] = ['id', 'value', 'fec_inicio', 'fec_vencida','Usuario','estado','accion'];
    dataSource: MatTableDataSource<tarea>;
  
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;
  
    constructor(
      public tareaServices: TareaService
    ) {
      // Create 100 users
     //const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
  
      // Assign the data to the data source for the table to render
     
    }
  
    ngOnInit() {
      this.AbrirTareas();
      
      
    }

    AbrirTareas(){
      return this.tareaServices.TodasTareas().subscribe((data:{})=>{
        this.datos=data;
        if(this.datos!=null){
          this.dataSource = new MatTableDataSource(this.datos);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      })
    }
    BorrarTarea(data){
      var map=this.datos.map(x=>{return x.id});
      var index=map.indexOf(data.id);
      return this.tareaServices.EliminarTarea(data.id).subscribe(res=>{
        this.datos.splice(index, 1)
        this.dataSource = new  MatTableDataSource(this.datos);
        console.log('Tarea Eliminada')
      })
      console.log(this.dataSource);
  
    }
    applyFilter(filterValue: string) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
  }
  
  /** Builds and returns a new User. */
  function createNewUser(id: number): UserData {
    const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
        NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
  
    return {
      id: id.toString(),
      name: name,
      progress: Math.round(Math.random() * 100).toString(),
      color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
    };
  }


