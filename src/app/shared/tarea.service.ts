import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tarea } from './tarea';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TareaService {

  //base url desde donde se consume la api
  //baseurl='http://aartback.ddns.net:5000/'
  baseurl='http://localhost:5000/'

  constructor(private http:HttpClient) { }

  httpOptions ={
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  //POST
  CrearTarea(data): Observable<tarea>{
    return this.http.post<tarea>(this.baseurl+'api/todos/',JSON.stringify(data),this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  //get seleccionar todos
  TodasTarea(id):Observable<tarea>{
    return this.http.get<tarea>(this.baseurl+'api/todos/'+id)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }
  //get seleccionar todos
  TodasTareas():Observable<tarea>{
    return this.http.get<tarea>(this.baseurl+'api/todos/')
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  //put Actulizar datos
  ActualizarTarea(data):Observable<tarea>{
    return this.http.put<tarea>(this.baseurl+'api/todos/',JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  //Eliminar tarea
  EliminarTarea(id){
    return this.http.delete<tarea>(this.baseurl+'api/todos/'+id,this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl) 
    )
  }

 // Error handling
 errorHandl(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  console.log(errorMessage);
  return throwError(errorMessage);
  }

}
