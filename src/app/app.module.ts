import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { RouterModule, Route, Routes } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MaterialComponente} from './materialComponent';
import { CrearTareaComponent } from './crear-tarea/crear-tarea.component';
import { AccesoComponent } from './acceso/acceso.component';
import { RegistroUsuarioComponent } from './registro-usuario/registro-usuario.component';
import {TareaService} from './shared/tarea.service';
import { ObtenerTareaComponent } from './obtener-tarea/obtener-tarea.component';
import { EditarTareaComponent } from './editar-tarea/editar-tarea.component';

import { TableOverviewExampleComponent } from './table-overview-example/table-overview-example.component';


const routes: Route []= 
[
  
  {path: 'registrar', component: CrearTareaComponent},
  {path: 'login', component: AccesoComponent},
  {path: 'nuevoUsuario', component: RegistroUsuarioComponent},
  {path: 'acceso', component: AccesoComponent},
  {path: 'obtenerTarea', component: ObtenerTareaComponent},
  {path: 'EditarTarea/:para', component: EditarTareaComponent},
  {path: 'tabla', component: TableOverviewExampleComponent}


  
];

@NgModule({
  declarations: [
    AppComponent,
    CrearTareaComponent,
    AccesoComponent,
    RegistroUsuarioComponent,
    ObtenerTareaComponent,
  
    EditarTareaComponent,   TableOverviewExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialComponente,
    NgbModule,
    
    RouterModule.forRoot(routes),
    HttpClientModule ,FormsModule, ReactiveFormsModule
    

  ],
  providers: [TareaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
