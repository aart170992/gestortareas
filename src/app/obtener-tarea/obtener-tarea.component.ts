import { Component, OnInit } from '@angular/core';
import {TareaService} from '../shared/tarea.service';

@Component({
  selector: 'app-obtener-tarea',
  templateUrl: './obtener-tarea.component.html',
  styleUrls: ['./obtener-tarea.component.css']
})
export class ObtenerTareaComponent implements OnInit {

  TareaList: any =[];
  
  constructor(
    public tareaServices: TareaService
  ) { }

  ngOnInit() {
    this.AbrirTareas();
  }

  AbrirTareas(){
     
    return this.tareaServices.TodasTareas().subscribe((data:{})=>{
      this.TareaList=data;
    })
    
  }

  BorrarTarea(data){
    var map=this.TareaList.map(x=>{return x.id});
    var index=map.indexOf(data.id);
    return this.tareaServices.EliminarTarea(data.id).subscribe(res=>{
      this.TareaList.splice(index, 1)
      console.log('Tarea Eliminada')
    })

  }
}
