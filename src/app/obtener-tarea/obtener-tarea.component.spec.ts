import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObtenerTareaComponent } from './obtener-tarea.component';

describe('ObtenerTareaComponent', () => {
  let component: ObtenerTareaComponent;
  let fixture: ComponentFixture<ObtenerTareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObtenerTareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObtenerTareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
