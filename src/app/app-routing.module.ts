import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccesoComponent} from './acceso/acceso.component'


const routes: Routes = [
  { path: 'login', component: AccesoComponent,  pathMatch: 'full'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
